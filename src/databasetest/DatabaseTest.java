/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasetest;

import com.sun.org.apache.xalan.internal.xsltc.compiler.util.ErrorMsg;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author SI-MEXON
 */
public class DatabaseTest {

    static Connection dbConnection;

    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            dbConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/students", "root", "funaibsc151422");
            Statement statement = dbConnection.createStatement();
            String tableName = "test_table";

            System.out.print("id");
            System.out.print("\t");
            System.out.print("name");
            System.out.print("\t\t");
            System.out.print("gender");
            System.out.print("\t");
            System.out.print("date_of_birth");
            System.out.println("\n");
            int numTimes = 3;
            for (int i = 0; i < numTimes; i++) {
                ResultSet result = statement.executeQuery("select * from test_table");
                while (result.next()) {
                    System.out.print(result.getInt("id"));
                    System.out.print("\t");
                    System.out.print(result.getString("name"));
                    System.out.print("\t");
                    System.out.print(result.getString("gender"));
                    System.out.print("\t");
                    System.out.print(result.getDate("date_of_birth"));
                    System.out.print("\n");
//                 break;
//                 result.beforeFirst();
                }
                result.close();
                System.out.println("");
            }

        } catch (ClassNotFoundException ex) {
            System.out.println("An exception (1) has occured");
            Logger.getLogger(DatabaseTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            System.out.println("An exception (2) has occured");
            Logger.getLogger(DatabaseTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            System.out.println("An exception (3) has occured");
            Logger.getLogger(DatabaseTest.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            System.out.println("An exception (4) has occured");
            Logger.getLogger(DatabaseTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
